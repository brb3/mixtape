# <img src="media/mixtape.png" alt="mixtape" width="150"/>

[![Netlify Status](https://api.netlify.com/api/v1/badges/1b46db30-84d0-4235-920a-499c93986800/deploy-status)](https://app.netlify.com/sites/playlists/deploys)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)



## [Demo][demo]
<p align="center">
  <img src="https://gitlab.com/ryanmaynard/mixtape/raw/master/demo.png">
</p>

## Description

A simple Jekyll theme to host playlists, or any similar sort of directory.

## Usage

#### 1. Add playlist data to `_data/playlists.yml`

```yaml
- image_url: /media/images/freedive.jpg
  full_path: "https://open.spotify.com/user/ryanmaynard/playlist/5xdvjYRwAHYxzQzJf9IfR1?si=HERczhSeTlyxb-wzqW8New"
  title: "Freedive"
  tags: electronic, dance, chill, indiechill, chillwave, focus, work, study
  col-size: col-1

- image_url: /media/images/lean.jpg
  full_path: "https://open.spotify.com/user/ryanmaynard/playlist/3s6PKKZuYi8eqjG6x7WARt?si=fJCTLV4iQCe7aLsDGc0zdA2"
  title: "Lean Into It"
  tags: chillpop, upbeat, bpm, indiewave
  col-size: col-1

- image_url: /media/images/lofi.jpg
  full_path: "https://open.spotify.com/user/ryanmaynard/playlist/2Y3ujCGQtyVsH1oSkt0lG1?si=hv7lmX41SeOWZqSEyhSp7Q"
  title: "LoFi volume 423"
  tags: electronic, dance, chill, indiechill, chillwave, focus, work, study, lofi
  col-size: col-1

```
#### 2. Add album art to `media/images/`

#### 3. Update `_config.yml` as needed

#### 4. Ship it! 


### Alternative Beginner Option

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://gitlab.com/ryanmaynard/mixtape)

You can also just deploy this repo to Netlify as is by clicking the button above, and make changes to it later. 

## Todo

- Add analytics
- Add submission form
- Show tags and links on album hover
- Make genre pages that pull tags


## Contributing

Be sure to review the [Contributing Guide][contributing] before submitting a Merge Request.

1. Fork it `https://gitlab.com/ryanmaynard/mixtape/forks/new`
2. Create your feature branch `git checkout -b my-new-feature`
3. Commit your changes `git commit -am 'Add some feature'`
4. Push to the branch `git push origin my-new-feature`
5. Create a new Merge Request `https://gitlab.com/ryanmaynard/mixtape/merge_requests/new`

## License

[MIT TLDR][tldr]

[License Text][license]

[demo]: https://chattanoogamixtape.club
[contributing]: CONTRIBUTING.md
[tldr]: https://tldrlegal.com/license/mit-license
[license]: https://gitlab.com/ryanmaynard/mixtape/blob/master/LICENSE

